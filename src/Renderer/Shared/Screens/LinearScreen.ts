import AbstractScreen from "../AbstractScreen";
import { RequestParameters } from "../Requests";
import { TriggerableScreenOptions } from "../ScreenOptions";

const clss = {
    /** Pattern for creating the screen's current state classes */
    currentPattern: "screen__linear__phase-",
    /** Classes of individual phases */
    phase: {
        /** Base class for each phase */
        base: "linear-phase",
        /** All text phases must have this class */
        text: "linear-phase__text",
        /** An individual language class base */
        lang: "linear-phase-lang"
    },
    progressbarOverlap: "screen-progressbar__overlap"
};

/**
 * @deprecated
 */
class LinearScreen extends AbstractScreen
{

    params: LinearScreenOptions;

    video: HTMLVideoElement;
    source: HTMLSourceElement;
    videoContainer: HTMLDivElement;
    pre: HTMLDivElement;
    post: HTMLDivElement;

    countdown: HTMLDivElement;
    counter: number;

    timer_countdown: NodeJS.Timer;
    timer_pre: NodeJS.Timer;
    timer_post: NodeJS.Timer;

    onBuild(): void {}

    onRemove(): void {}

    onActivate(requestParameters: LinearScreenRequest ): void {

        // Create the text phases
        this.pre = this.buildTextPhaseDOM( "pre", this.params.pre );
        this.post = this.buildTextPhaseDOM( "post", this.params.post );

        this.counter = 0;

        // Create the video container
        this.videoContainer = document.createElement( "div" );
        this.videoContainer.classList.add( clss.phase.base );
        this.videoContainer.classList.add( clss.phase.base + "__video" );

        // Create the video element itself
        this.video = document.createElement( "video" );
        this.video.muted = true;
        this.source = document.createElement( "source" );
        this.source.setAttribute( "type", "video/mp4" );
        this.source.setAttribute( "src", this.params.video );
        this.video.appendChild( this.source );
        this.videoContainer.appendChild( this.video );

        // Create the countdown
        this.countdown = document.createElement( "div" );
        this.countdown.classList.add( "linear-countdown" );

        // Compose the DOM
        this.wrapper.appendChild( this.countdown );
        this.wrapper.appendChild( this.pre );
        this.wrapper.appendChild( this.videoContainer );
        this.wrapper.appendChild( this.post );

        // Load the video
        this.video.load();

        this.setPhase( "pre" );
        this.progressbar.classList.add( clss.progressbarOverlap );

        this.video.addEventListener( "loadedmetadata", e => {

            let times = {
                /** The pre duration is rounded because of the counter */
                pre: Math.floor( ( this.params.pre.duration / 1000 ) - 1 ) * 1000,
                post: this.params.post.duration,
                video: this.video.duration * 1000,
            }

            this.counter = times.pre / 1000;

            // Start the countdown
            this.timer_countdown = setInterval( () => {
                this.countdown.innerHTML = this.counter.toString();
                this.counter--;
            }, 1000 );

            // Start the progressbar
            this.runProgressbar( times.pre + times.post + times.video );

            // Start the first phase
            
            this.timer_pre = setTimeout( () => {

                this.setPhase( "video" );
                this.video.play();

                this.progressbar.classList.remove( clss.progressbarOverlap );

                // Stop the counter
                clearInterval( this.timer_countdown );
                this.countdown.remove();

            }, this.params.pre.duration );

        } );

        this.video.addEventListener( "ended", () => {
            this.setPhase( "post" );
            this.progressbar.classList.add( clss.progressbarOverlap );
            this.timer_post = setTimeout( ()=> {
                window.Controller.sendShowEnded();
            }, this.params.post.duration );
        } );
        
    }

    onDeactivate(): void {
        if ( this.timer_pre !== undefined ) {
            clearTimeout( this.timer_pre );
        };
        if ( this.timer_post !== undefined ) {
            clearTimeout( this.timer_post );
        };
        if ( this.timer_countdown !== undefined ) {
            clearTimeout( this.timer_countdown );
        }
        if ( this.source !== undefined ) {
            this.source.remove();
        }
        if ( this.video !== undefined ) {
            this.video.remove();
        }
        if ( this.videoContainer !== undefined ) {
            this.videoContainer.remove();
        }
        if ( this.pre !== undefined ) {
            this.pre.remove();
        }
        if ( this.post !== undefined ) {
            this.post.remove();
        }
        if ( this.countdown !== undefined ) {
            this.countdown.remove();
        }
        this.setPhase( undefined );
    }

    protected buildTextPhaseDOM( phase: "pre"|"post", params: LinearTextParameters ): HTMLDivElement
    {
        let c = document.createElement( "div" );
        c.classList.add( clss.phase.base );
        c.classList.add( clss.phase.text );
        c.classList.add( [clss.phase.base, phase]. join( "__" ) );

        c.appendChild( this.buildOneLanguage( "cz", params.content.cz ) );
        c.appendChild( this.buildOneLanguage( "en", params.content.en ) );

        return c;
    }

    protected buildOneLanguage( lang: "cz"|"en", paragraphs: string[] ): HTMLDivElement
    {
        let c = document.createElement( "div" );
        c.classList.add( clss.phase.lang );
        c.classList.add( [clss.phase.lang, lang].join("__") );

        paragraphs.map( p => {
            let el = document.createElement( "p" );
            el.innerHTML = p;
            c.appendChild( el );
        } );

        return c;
    }

    protected setPhase( phase: "pre"|"video"|"post"|undefined ): void
    {
        console.log( "nastavuji fázi " + phase );
        [ "pre", "video", "post" ].map( scope => {
            let cls = clss.currentPattern + scope;
            if ( scope === phase ) {
                this.container.classList.add( cls );
            } else {
                this.container.classList.remove( cls );
            }
        } );
    }

}

export default LinearScreen;

export interface LinearScreenOptions extends TriggerableScreenOptions {
    video: string,
    pre: LinearTextParameters,
    post: LinearTextParameters
}

interface LinearTextParameters {
    duration: number,
    content: {
        cz: string[],
        en: string[]
    }
}

export interface LinearScreenRequest extends RequestParameters {
    title: {
        cz: string,
        en: string,
    },
    video: string,
    pre: LinearTextParameters,
    post: LinearTextParameters
}