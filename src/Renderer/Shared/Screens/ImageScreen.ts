import AbstractScreen from "../AbstractScreen";
import { RequestParameters } from "../Requests";
import { FileScreenOptions } from "../ScreenOptions";

class ImageScreen extends AbstractScreen
{

    type: string = "image";

    defaults: {
        file: "bloud.jpg"
    }

    params: FileScreenOptions;

    image: HTMLImageElement;

    timer: NodeJS.Timer;

    onBuild(): void
    {

        this.image = document.createElement( "img" );

        this.image.setAttribute( "src", this.params.file );

        this.wrapper.appendChild( this.image );   
    }

    onRemove(): void
    {
        this.image.remove();
    }

    onActivate( requestParameters: ImageRequestParameters = { duration: 3000 } ): void {

        this.runProgressbar( requestParameters.duration );

        this.timer = setTimeout( () => {

            window.Controller.sendShowEnded();

        }, requestParameters.duration );
        
    }

    onDeactivate(): void {
        this.progressbar.style.width = "0px";
        if ( this.timer !== undefined ) {
            clearTimeout( this.timer );
            this.timer = undefined;
        }
    }

}

export default ImageScreen;

export interface ImageRequestParameters extends RequestParameters
{
    duration: number
}