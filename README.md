# Kiosk

## Keyboad shortuts

Key `Alt` shows the app menu

Key `r` reads `storage/config.yml` and reloads the application with recent configuration.

## Installation

### Environment

Dependencies:

```bash
# Install nvm
sudo apt install nvm
        
# Install nodejs 16.13.2
nvm install 16.13.2

# Use the desired node version
nvm use 16.13.2

# Install Yarn
npm install --global yarn
```

Git hook:
```bash
# install new dependencies if any
npm install
```

Start:
```
nvm use 16.13.2
npm run start
```

### HW setup

- make sure you have two displays:
    - "tablet" will be displayed on the primary display
    - "projector" will be displayed on the secondary display
- copy content of the `storage/` folder to `YOUR_HOME_DIR/storage/`
    - Windows: `C:\Users\USERNAME\storage\`
    - Linux: `home/USERNAME/storage/`
- optionally add more content & configuration - see content of the file `storage/config.yml` which serves as example on how to do it
- run the executable from `out/` directory

### Development

- navigate to the project's root folder & run:
    - `yarn` - installs all JS dependencies
    - `yarn start` - runs the app in development mode
    - `yarn package` - packages the application for your platform into `out/` directory
    - `yarn make` - makes an installable file into `out/` directory
- how to toggle development mode?
    - `src/index.ts:25` variable DEBUG indicates if the app is in debug mode