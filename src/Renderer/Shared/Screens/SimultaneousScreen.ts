import AbstractScreen from "../AbstractScreen";
import { RequestParameters } from "../Requests";
import { TriggerableScreenOptions } from "../ScreenOptions";

const clss = {
    /** Pattern for creating the screen's current state classes */
    currentPattern: "screen__simultaneous__phase-",
    /** Classes of individual phases */
    phase: {
        /** Base class for each phase */
        base: "simultaneous-phase",
        /** Text part of the phase should have this class */
        text: "simultaneous-phase-text",
        /** Media part of the phase should have this class */
        media: "simultaneous-phase-media",
        /** An individual language class base */
        lang: "simultaneous-phase-text-lang"
    },
    progressbarOverlap: "screen-progressbar__overlap"
};


class SimultaneousScreen extends AbstractScreen
{

    params: SimultaneousScreenOptions;

    pre: PhaseStructure;
    post: PhaseStructure;

    icon: HTMLImageElement;

    timer_post: NodeJS.Timer;

    onBuild(): void {}

    onRemove(): void {}

    onActivate(requestParameters: SimultaneousScreenOptions ): void {

        // Build phases
        this.pre = this.buildPhaseDOM( "pre" );
        this.post = this.buildPhaseDOM( "post" );

        this.icon = document.createElement( "img" );
        this.icon.classList.add( "simultaneous-screen-icon" );
        this.icon.src = this.params.icon_screen;

        this.wrapper.appendChild( this.icon );


        // Provide video content
        this.pre.video = document.createElement( "video" );
        this.pre.source = document.createElement( "source" );
        this.pre.source.setAttribute( "src", this.params.video );

        this.pre.video.appendChild( this.pre.source );
        this.pre.media.appendChild( this.pre.video );

        // Provide image content
        this.post.image = document.createElement( "img" );
        this.post.image.setAttribute( "src", this.params.image );

        this.post.media.appendChild( this.post.image );

        this.wrapper.append( this.pre.container, this.post.container );

        
        // Start phase one
        this.pre.video.addEventListener( "loadedmetadata", e => {

            let times = {
                /** The pre duration is rounded because of the counter */
                pre: this.pre.video.duration * 1000,
                post: this.params.post.duration,
            }

            // Start the progressbar
            this.runProgressbar( times.pre + times.post );

            this.pre.video.play();

            this.setPhase( "pre" );

        } );

        this.pre.video.addEventListener( "ended", () => {

            this.setPhase( "post" );

            this.progressbar.classList.add( clss.progressbarOverlap );

            this.timer_post = setTimeout( ()=> {
                window.Controller.sendShowEnded();
            }, this.params.post.duration );
        } );
        
    }

    onDeactivate(): void 
    {

        this.removePhase( this.pre );
        this.removePhase( this.post );

        if ( this.timer_post !== undefined ) {
            clearTimeout( this.timer_post );
        }

        this.setPhase( undefined );
        this.progressbar.classList.remove( clss.progressbarOverlap );

    }

    /**
     * Build the phase dom
     */
    protected buildPhaseDOM( phase: "pre"|"post" ): PhaseStructure
    {

        // Container
        let c = document.createElement( "div" );
        c.classList.add( clss.phase.base );
        c.classList.add( [clss.phase.base, phase ].join("__") );

        // Text
        let text = document.createElement( "div" );
        text.classList.add( clss.phase.text );

        // Text content of the phase
        let cz = this.buildPhaseLangText( "cz", phase );
        let en = this.buildPhaseLangText( "en", phase );
        
        // Media
        let media = document.createElement( "div" );
        media.classList.add( clss.phase.media );

        // Composition
        c.appendChild( media );
        c.appendChild( text );
        text.appendChild( cz );
        text.appendChild( en );

        return {
            container: c,
            text: text,
            cz: cz,
            en: en,
            media: media,
            video: undefined,
            source: undefined,
            image: undefined
        } as PhaseStructure;

    }

    /**
     * Builds textual content for one individual language in one individual phase
     */
    protected buildPhaseLangText( lang: "cz"|"en", phase: "pre"|"post" ): HTMLDivElement {

        let title = this.params.title[ lang ];
        let paragraphs = this.params[ phase ].content[ lang ];

        let c = document.createElement( "div" );
        c.classList.add( clss.phase.lang );
        c.classList.add( [ clss.phase.lang, lang ].join( "__" ) );

        let t = document.createElement( "div" );
        t.innerHTML = title;

        c.appendChild( t );
        
        paragraphs.forEach( p => {
            let par = document.createElement( "p" );
            par.innerHTML = p;
            c.appendChild( par );
        } );

        return c;
        
    }


    protected removePhase( phase: PhaseStructure ): this
    {

        if ( phase !== undefined ) {
            this
                .removeElement( this.icon )
                .removeElement( phase.source )
                .removeElement( phase.video )
                .removeElement( phase.image )
                .removeElement( phase.cz )
                .removeElement( phase.en )
                .removeElement( phase.text )
                .removeElement( phase.media )
                .removeElement( phase.container );
        }

        return this;

    }


    protected removeElement( el: HTMLElement ): this
    {
        if ( el !== undefined ) {
            el.remove();
        }
        return this;
    }


    protected setPhase( phase: "pre"|"post"|undefined ): void
    {
        [ "pre", "post" ].map( scope => {
            let cls = clss.currentPattern + scope;
            if ( scope === phase ) {
                this.container.classList.add( cls );
            } else {
                this.container.classList.remove( cls );
            }
        } );
    }

}

export default SimultaneousScreen;

interface PhaseStructure {
    container: HTMLDivElement,
    media: HTMLDivElement,
    video?: HTMLVideoElement,
    source?: HTMLSourceElement,
    image?: HTMLImageElement
    text: HTMLDivElement,
    cz: HTMLDivElement,
    en: HTMLDivElement,
}

export interface SimultaneousScreenOptions extends TriggerableScreenOptions {
    video: string,
    image: string,
    pre: SimultaneousTextParameters,
    post: SimultaneousTextParameters
}

interface SimultaneousTextParameters {
    duration: number,
    content: {
        cz: string[],
        en: string[]
    }
}

export interface SimultaneousScreenRequest extends RequestParameters {
    title: {
        cz: string,
        en: string,
    },
    video: string,
    image: string,
    pre: SimultaneousTextParameters,
    post: SimultaneousTextParameters
}