const { ipcRenderer } = window.require( "electron" );
import { ConfigInterface } from "../../Main/Storage";
import Controller from "../Shared/Controller";
import { RequestParameters } from "../Shared/Requests";
import ScreensaverScreen, { ScreensaverScreenRequest } from "../Shared/Screens/ScreensaverScreen";
import TouchScreen from "../Shared/Screens/TouchScreen";

import "./tablet.scss";

const controller = new Controller( "app", "projector" );

declare global {
    interface Window { Controller: Controller }
}

window.Controller = controller;


// Listen for key events
document.addEventListener( "keydown", event => {

    if ( event.key === "r" ) {
        ipcRenderer.send( "request-content", "🤡 Tablet requested content!" );
    }

} );

// Set events upon main response
// Build the content
ipcRenderer.on( "respond-content", ( event, config: ConfigInterface ) =>  {

    controller.reset();

    controller.setConfig( config );

    // Add & Configure the screensaver

    console.log( config );

    let screensaver = controller.addScreensaverScreen( config.tablet.hand );
    
    screensaver.setClickCallback( controller.activateTouchControl.bind( controller ) );

    // Add & Configure the touch control
    controller.addTouchScreen();

    controller.activateScreensaver();

} );


ipcRenderer.on( "remove-highlight", (event, arg) => {
    let touch = controller.screens["touch"] as TouchScreen;
    touch.hightlightStop();
} );


// Trigger the content loading on the start
window.addEventListener( "load", () => {
    ipcRenderer.send( "request-content", "🤡 Tablet requested content!" );
} );

// Listen to routed playback
ipcRenderer.on( "signal", (event, args: RequestParameters ) => {
    controller.activateScreen( args.id, args );
} );