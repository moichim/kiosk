import AbstractScreen from "../AbstractScreen";
import { RequestParameters } from "../Requests";
import { ScreenOptions } from "../ScreenOptions";
import Controller from "../Controller";

import Typed from 'typed.js';


const PADDING = {
    x: 50,
    y: 10
}

const MAX_WIDTH = 500;

const SIZING: NumericalRange = {
    min: 0.8,
    max: 1.2
};

const ROTATIONS: NumericalRange = {
    min: -10,
    max: 10
}

const JUMPING: NumericalRange = {
    min: 3000,
    max: 15000
}

const DELAY: number = 2000;

interface NumericalRange {
    min: number,
    max: number
}

/**
 * @deprecated
 */
export default class ScreensaverScreen extends AbstractScreen
{
    type: string = "screensaver";

    params: ScreensaverScreenOptions;

    cz: string;
    en: string;

    imagesContainer: HTMLDivElement;
    images: Sprite[];

    slogan: Slogan;

    

    constructor( controller: Controller, params: ScreensaverScreenOptions ) {
        
        super( controller, params );
        
        this.cz = params.cz;
        this.en = params.en;

    }

    onBuild(): void 
    {

        // Create the images container
        this.imagesContainer = document.createElement( "div" );
        this.imagesContainer.classList.add( "screensaver-images" );

        this.wrapper.appendChild( this.imagesContainer );

    }

    onRemove(): void
    {

        if ( this.imagesContainer !== undefined ) {
            this.imagesContainer.remove();
        }

        if ( this.slogan !== undefined ) {
            this.slogan.remove();
        }
    }

    onActivate( request: ScreensaverScreenRequest ): void
    {

        this.images = [];

        let i = 0;

        for ( let image of this.controller.config.screensaver ) {

            let sprite = new Sprite( image, i );
            this.images.push( sprite );
            this.imagesContainer.appendChild( sprite.element );
            
            setTimeout( () => {
                sprite.init();
            }, DELAY * i );

            i++;

        }

        let cz = request.cz !== undefined && request.cz !== ""
            ? request.cz
            : this.cz;
        
        let en = request.en !== undefined && request.en !== ""
            ? request.en
            : this.en;

        this.slogan = new Slogan( cz, en );

        this.slogan.jump();

        this.wrapper.appendChild( this.slogan.element );

        this.slogan.init();

    }

    onDeactivate(): void {

        this.images.map( s => s.remove() );

        this.slogan.remove();

        this.images = [];
    }

}

abstract class Generative {

    protected getRandomUnit( min: number, max: number, unit: string, floor: boolean = false ): string
    {
        return this.getRandomNumber( min, max, floor ) + unit;
    } 

    protected getRandomNumber( min: number, max: number, floor: boolean = false ): number
    {
        let num = min + ( Math.random() * ( max - min ) );

        return ( 
            floor === true
                ? Math.floor( num )
                : num 
            );
    }

}

class Slogan extends Generative {

    element: HTMLDivElement;
    content: HTMLDivElement;

    cz: string;
    en: string;

    typed: Typed;

    constructor( cz: string, en: string = undefined ) {

        super();

        this.cz = cz;
        this.en = en;

        this.element = document.createElement( "div" );
        this.element.classList.add( "slogan" );

        this.content = document.createElement( "div" );
        this.content.classList.add( "slogan-content" );

        this.element.appendChild( this.content );
        this.element.appendChild( document.createElement( "br" ) );

    }

    jump() {
        this.element.style.top = this.getRandomUnit( 10, 70, "%" );
        this.element.style.left = this.getRandomUnit( 10, 30, "%" );
    }

    init() {


        let string = "<strong>" + this.cz+ "</strong>";

        if ( this.en !== undefined ) {
            string += "<span>";
            string += this.en;
            string += "</span>";
        }


        let options = {
            strings: [ string ],
            startDelay: 1000,
            typeSpeed: 40,
            backDelay: 3000,
            backSpeed: 10,
            loop: true,
            onLastStringBackspaced: () => {
                this.jump();
            }
        };

        this.typed = new Typed( this.content, options );

    }

    remove() {

        this.typed.destroy();

        this.content.remove();

        this.element.remove();

    }
}




class Sprite extends Generative {

    config: SpriteConfig;

    index: number;

    element: HTMLDivElement;
    image: HTMLImageElement;

    timer_jump: NodeJS.Timer;

    constructor( config: SpriteConfig, index: number) {

        super();

        this.index = index;

        this.image = document.createElement( "img" );
        this.image.classList.add( "sprite-image" );
        this.image.setAttribute( "src", config.image );
        this.image.style.maxWidth = MAX_WIDTH + "px";
        this.image.style.maxHeight = "auto";

        

        this.element = document.createElement( "div" );
        this.element.classList.add( "sprite" );
        this.element.classList.add( "sprite__index-" + index );

        this.element.style.top = ( ( window.innerHeight / 2 ) - this.image.height / 4 )+ "px";
        this.element.style.left = ( ( window.innerWidth / 2 ) - this.image.width / 4 )+ "px";

        this.element.setAttribute( "id", "sprite_" + index );

        this.element.appendChild( this.image );

        this.jump( this.getRandomNumber( JUMPING.min, JUMPING.max, true ) );
        

    }

    init() {

        // this.jump( this.getRandomNumber( JUMPING.min, JUMPING.max, true )  );

        setTimeout( () => {
            
            this.jump( this.getRandomNumber( JUMPING.min, JUMPING.max, true ) );

            this.image.classList.add( "on" );

            this.loop();
            
        }, this.index * DELAY );
    }

    remove() {
        
        this.image.remove();
        this.element.remove();

        if ( this.timer_jump !== undefined ) {
            clearTimeout( this.timer_jump );
            this.timer_jump = undefined;
        }

    }

    loop() {

        this.timer_jump = setTimeout(
            () => {
                this.jump( this.getRandomNumber( JUMPING. min, JUMPING.max, true )  );
                this.loop();
            }, 
            this.getRandomNumber( JUMPING. min, JUMPING.max, true ) 
        );
    }

    jump( duration: number ) {

        let transforms: string[] = [];

        // Rotation
        transforms.push( 
            "rotate( " + this.getRandomUnit( ROTATIONS.min, ROTATIONS.max, "deg" ) + ")"
        );

        // Scale
        transforms.push(
            "scale("+this.getRandomUnit( SIZING.min, SIZING.max, "" )+")"
        );

        // Translate
        let minX = - ( window.innerWidth / 2 ) + PADDING.x + ( MAX_WIDTH / 2 );
        let maxX = ( window.innerWidth / 2 ) - PADDING.x - ( MAX_WIDTH / 2 );

        let minY = - ( window.innerHeight / 2 ) - PADDING.y + ( this.image.height / 4 );
        let maxY = ( window.innerHeight / 2 ) + PADDING.y + ( this.image.height / 4 );

        transforms.push(
            "translate(" + this.getRandomUnit( minX,maxX, "px" )+ ", " + this.getRandomUnit( minY, maxY, "px" ) +")"
        );

        

        this.element.style.transition = [
            "all", 
            ( duration / 1000 ) + "s", 
            "ease-in-out" 
        ].join( " " );

        setTimeout( () => {
            this.element.style.transform = transforms.join( " " );
        }, 10 );

    }

    

}

interface SpriteConfig {
    image: string,
    max: number
}

export interface ScreensaverScreenOptions extends ScreenOptions {
    cz: string,
    en?: string
}

export interface ScreensaverScreenRequest extends RequestParameters {
    cz?: string,
    en?: string
}