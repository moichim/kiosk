import path from "path";
const yaml = require( 'js-yaml' );
import fs from "fs";

import { app } from "electron";

import { ScreenOptions } from "../Renderer/Shared/ScreenOptions";
import { getRandomString } from "../Utils/randomString";

/** 
 * The following fields in config.yml > screens shall be automatically converted to absolute URLs 
 */
const CONVERTIBLE_FIELDS:string[] = [
    "file", "background", "image", "video", "icon_trigger", "icon_screen", "one", "two"
];

export function getAbsolutePath( relative_path: string ): string {

    let cesta = path.join(
        app.getPath( "home" ),
        "storage",
        relative_path
    );

    return cesta;
}

export function getFileUrl( relative_path: string ): string {

    let separator = [1,2,3,4,5,6].map( i=>path.sep ).join("");

    return "storage:" + separator + getAbsolutePath( relative_path );
}

export function readConfigFile( file = "config.yml" ): ConfigInterface
{
    try {

        let content = yaml.load(
            fs.readFileSync( getAbsolutePath( file ), "utf-8" )
        ) as ConfigInterface;

        content.tablet.background = getFileUrl( content.tablet.background );
        content.tablet.hand = getFileUrl( content.tablet.hand );

        content.screensaver = content.screensaver.map( i => {
            return {
                image: getFileUrl( i.image ),
                max: i.max
            }
        } );

        // Map files to absolute paths
        content.screens = content.screens.map( screen => {

            CONVERTIBLE_FIELDS.map( f => {
                if ( screen.hasOwnProperty( f ) ) {
                    screen[ f ] = getFileUrl( screen[ f ] );
                }
            } );
            return screen;

        } );

        let newscreens = {} as {
            [index:string]: ScreenOptions
        };

        // 
        content.screens = content.screens.map( screen => {

            if ( ! screen.hasOwnProperty( "id" ) ) {
                screen.id = getRandomString( 5 );
            }

            newscreens[ screen.id ] = screen;

            return screen;

        });

        content.data = newscreens;

        return content;

        // map any files to absolute paths
    } catch ( e ) {
        console.error( e );
    }
}



export type ConfigInterface = {
    tablet?: {
        background: string,
        hand: string,
        screensaver?: {
            cz?: string,
            en?: string
        }
    },
    projector?: {
        screensaver?: {
            cz?: string,
            en?: string
        }
    }
    screens: ScreenOptions[],
    data: {
        [index:string]: ScreenOptions
    },
    screensaver: { image: string, max: number }[]
}

