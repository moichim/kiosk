import { app, protocol, session } from "electron";

const path = require( "path" );

app.whenReady().then( () => {

    /**
     * Register the storage:/// protocol to access local files
     */
    protocol.registerFileProtocol(
        "storage",
        (request, callback) => {
            const url = request.url.substr(10);
            callback(decodeURI(path.normalize(url)));
        }
    );

    /**
     * Enable storage:/// in src="" atributes
     * 
     * @link https://www.electronjs.org/docs/latest/tutorial/security#7-define-a-content-security-policy
     * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy
     */
    session.defaultSession.webRequest.onHeadersReceived((details, callback) => {
        callback({
            responseHeaders: {
                ...details.responseHeaders,
                'Content-Security-Policy': [
                    // "img-src storage: filesystem: 'unsafe-eval' 'unsafe-inline' 'self'",
                    // "media-src storage: filesystem: 'unsafe-eval' 'unsafe-inline' 'self'",
                    // "style-src storage: filesystem: 'unsafe-eval' 'unsafe-inline' 'self'"
                    "img-src storage:",
                    "media-src storage:",
                    "style-src storage: 'unsafe-inline'"
                ]
            }
        })
    });


} );