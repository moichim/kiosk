import { getRandomString } from "../../Utils/randomString";
import Controller from "./Controller";
import { RequestParameters } from "./Requests";
import { ScreenOptions } from "./ScreenOptions";

const classes = {
    base: "screen",
    active: "screen__active",
    inactive: "screen__inactive" 
};

abstract class AbstractScreen {

    type: string;

    id: string;

    click_callback: CallableFunction;

    /** Override the default parameters */
    defaults: Object;

    /** The actual parameters. */
    params: ScreenOptions;

    /** Reference to the main app */
    controller: Controller;

    /** The main container */
    container: HTMLDivElement;

    /** The content shall be all put here */
    wrapper: HTMLDivElement;

    /** The bar is omnipresent */
    progressbar: HTMLDivElement;

    /**
     * @param controller The global window's controller must be provided as reference
     * @param params See ScreenOptions.js for details
     */
    constructor(
        controller: Controller,
        params: ScreenOptions
    ) {

        // Merge the provided parameters with defaults
        this.params = {
            ...this.defaults,
            ...params
        };

        this.controller = controller;

        // Genrate the ID
        this.id = this.params.id !== undefined
            ? this.params.id
            : getRandomString();

        // Build the container
        this.container = document.createElement( "div" );
        this.container.id = this.id;
        this.container.classList.add( classes.base );
        this.container.classList.add( classes.base + "__" +this.params.type );

        this.wrapper = document.createElement( "div" );
        this.wrapper.classList.add( "screen-wrapper" );

        this.container.appendChild( this.wrapper );

        this.progressbar = document.createElement( "div" );
        this.progressbar.classList.add( "screen-progressbar" );

        // this.progressbar.style.height = "10px";

        // Add the bar
        this.container.appendChild( this.progressbar );

        // Append the container to the app
        this.controller.element.appendChild( this.container );

        this.onBuild();

        // Add event listeners
        this.wrapper.addEventListener( "click", ( event ) => {
            if ( this.click_callback !== undefined ) {
                this.click_callback();
            }
        } );

    }

    abstract onBuild(): void;

    abstract onRemove(): void;

    abstract onActivate( requestParameters:RequestParameters ): void;

    abstract onDeactivate(): void;

    remove(): void
    {
        this.onRemove();
        this.container.remove();
    }

    activate( requestParameters: RequestParameters ): void
    {
        
        this.container.style.display = "block";

        setTimeout( () => {
            this.addClass( classes.active );
            this.removeClass( classes.inactive );
            this.onActivate( requestParameters );
        }, 100 );
    }

    deactivate(): void
    {
        this.addClass( classes.inactive );
        this.removeClass( classes.active );
        this.onDeactivate();

        setTimeout( () => {
            this.container.style.display = "none";
            this.progressbar.style.width = "0px";
        }, 300 );
    }


    addClass( cls: string ) {
        this.container.classList.add( cls );
    }

    removeClass( cls: string ) {
        this.container.classList.remove( cls );
    }

    setClickCallback(
        callable: CallableFunction
    ): this
    {
        this.click_callback = callable;
        return this;
    }

    runProgressbar( duration: number ): this
    {

        this.progressbar.animate(
            [
                { width: "0%" },
                { width: "100%" }
            ],
            {
                duration: duration
            }
        );

        return this;
    }



}

export default AbstractScreen;

