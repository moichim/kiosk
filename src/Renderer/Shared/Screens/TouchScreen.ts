import AbstractScreen from "../AbstractScreen"
import Controller from "../Controller";
import { RequestParameters } from "../Requests";
import { ScreenOptions, TriggerableScreenOptions } from "../ScreenOptions";

export default class TouchScreen extends AbstractScreen
{

    triggers: {
        [index: string]: Trigger
    };

    params: TouchScreenOptions;

    trigger_wrapper: HTMLDivElement;
    background: HTMLDivElement;
    title: HTMLDivElement;
    heading: HTMLDivElement;
    cz: HTMLDivElement;
    en: HTMLDivElement;

    highlight: Trigger;
    

    onBuild(): void {

        // Build the background
        this.background = document.createElement( "div" );
        this.background.classList.add( "touch-background" );

        let image = document.createElement( "img" );
        image.setAttribute( "src", this.params.background );
        
        this.background.appendChild( image );

        this.background.addEventListener( "click", () => {

            if ( this.highlight === undefined ) {
                window.Controller.timerStart();
            }

        } );

        this.wrapper.appendChild( this.background );


        // Build the trigger wrapper
        this.trigger_wrapper = document.createElement( "div" );
        this.trigger_wrapper.classList.add( "trigger-wrapper" );

        this.wrapper.appendChild( this.trigger_wrapper );


        // Build individual triggers
        this.triggers = {};

        for ( let screen of this.params.screens ) {
            
            if ( screen.hasOwnProperty( "trigger" ) ) {
                let trigger = new Trigger( this, screen as TriggerableScreenOptions );
                this.triggers[ trigger.id ] = trigger;
            }
        }


        // Build the title
        this.title = document.createElement( "div" );
        this.title.classList.add( "touch-title" );
        this.title.classList.add( "touch-title__idle" );

        // Build the wrapper of titles
        this.heading = document.createElement( "div" );
        this.heading.classList.add( "touch-title-heading" );

        // Build titles for individual languages
        this.cz = document.createElement( "div" );
        this.cz.classList.add( "touch-title-heading-cz" );
        this.en = document.createElement( "div" );
        this.en.classList.add( "touch-title-heading-en" );

        // Ciompose the DOM
        this.heading.appendChild( this.cz );
        this.heading.appendChild( this.en );
        this.title.appendChild( this.heading );
        this.wrapper.appendChild( this.title );
        
    }

    onRemove(): void {
        this.trigger_wrapper.remove();
        this.triggers = {};
    }

    onActivate(requestParameters: RequestParameters): void {
        
    }

    onDeactivate(): void {
        
    }

    highlightStart( trigger: Trigger ): void {

        let pass = false;

        if ( this.highlight === undefined ) {
            pass = true;
        } else {
            if ( this.highlight.id !== trigger.id ) {
                pass = true;
            }
        }

        if ( !pass ) {
            return;
        }

        this.highlight = trigger;

        this.controller.sendRequest( trigger.id );

        for ( let t in this.triggers ) {
            let tr = this.triggers[ t ];
            if ( tr.id !== trigger.id ) {
                tr.element.classList.remove( "trigger__highlight" );
                tr.icon.src = tr.config.icon_trigger;
            }
        }

        this.container.classList.add( "screen__touch__highlight" );

        this.cz.innerHTML = trigger.config.title.cz; 
        this.en.innerHTML = trigger.config.title.en;
        this.title.classList.remove( "touch-title__idle" );
        this.title.classList.add( "touch-title__active" );

        trigger.element.classList.add( "trigger__highlight" );

        this.highlight.icon.src = this.highlight.config.icon_screen;

        window.Controller.timerStop();

    }

    hightlightStop(): void {

        this.container.classList.remove( "screen__touch__highlight" );

        this.highlight.element.classList.remove( "trigger__highlight" );

        this.highlight.icon.src = this.highlight.config.icon_trigger;

        this.highlight = undefined;
        
        this.title.classList.remove( "touch-title__idle" );
        this.title.classList.add( "touch-title__active" );

        setTimeout( ()=>{
            this.cz.innerHTML = "";
            this.en.innerHTML = "";
        }, 300 );

        window.Controller.timerStart();

    }

}

export class Trigger {
    
    screen: TouchScreen;

    config: TriggerableScreenOptions;

    element: HTMLDivElement;

    icon: HTMLImageElement;

    id: string;

    constructor(
        screen: TouchScreen,
        config: TriggerableScreenOptions
    ) {

        this.screen = screen;
        this.config = config;
        this.id = config.id;

        this.element = document.createElement( "div" );
        this.element.id = "trigger_" + this.id;

        // Store czech title
        this.element.setAttribute( "title", config.title.cz );

        this.element.appendChild( document.createElement( "div" ) );

        this.icon = document.createElement( "img" );
        this.icon.src = this.config.icon_trigger;
        this.element.appendChild( this.icon );

        // Position
        this.element.style.top = ( config.trigger.y - config.trigger.width/2 ).toString().concat( "px" );
        this.element.style.left = ( config.trigger.x - config.trigger.width/2 ).toString().concat( "px" );

        // Dimensions
        this.element.style.height = config.trigger.width + "px";
        this.element.style.width = config.trigger.width + "px";

        this.element.classList.add( "trigger" );

        this.screen.trigger_wrapper.appendChild( this.element );

        this.element.addEventListener( "click", () => {

            this.screen.highlightStart( this );

        } );

    }

}

export interface TouchScreenOptions extends ScreenOptions {
    screens: ScreenOptions[],
    background: string
}

export interface TouchScreenRequest {
    highlight?: string
}