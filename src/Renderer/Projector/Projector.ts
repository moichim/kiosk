const { ipcRenderer } = window.require( "electron" );
import { ConfigInterface } from "../../Main/Storage";
import Controller from "../Shared/Controller";
import { RequestParameters } from "../Shared/Requests";

import "./projector.scss";

const controller = new Controller( "app", "tablet" );

declare global {
    interface Window { Controller: Controller }
}

window.Controller = controller;

// Listen for key events
document.addEventListener( "keydown", event => {

    if ( event.key === "r" ) {
        ipcRenderer.send( "request-content", "🤡 Projector requested content!" );
    }

} );

// Set events upon main response
// Build the content
ipcRenderer.on( "respond-content", ( event, config: ConfigInterface ) =>  {

    controller.reset();

    controller.setConfig( config );

    for ( let screen of config.screens ) {
        controller.createScreen( screen );
    }

    controller.addScreensaverScreen();

    controller.activateScreensaver();

} );

// trigger the content loading on the start
window.addEventListener( "load", () => {
    ipcRenderer.send( "request-content", "🤡 Tablet requested content!" );
} );

// Listen to routed playback
ipcRenderer.on( "signal", (event, args: RequestParameters ) => {
    controller.activateScreen( args.id, args );
} );