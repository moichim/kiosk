import AbstractScreen from "../AbstractScreen";
import { FileScreenOptions } from "../ScreenOptions";

class VideoScreen extends AbstractScreen {

    type: string = "video";

    params: FileScreenOptions;

    video: HTMLVideoElement;
    source: HTMLSourceElement;

    onBuild(): void {}

    onRemove(): void {}

    onActivate(): void {

        // Create the video element
        this.video = document.createElement( "video" );

        this.video.classList.add( "video-element" );
        this.video.muted = true;

        // Crteate the source element
        this.source = document.createElement( "source" );
        this.source.classList.add( "video-source" );
        this.source.setAttribute( "type", "video/mp4" );
        this.source.setAttribute( "src", this.params.file );

        this.video.appendChild( this.source );
        this.wrapper.appendChild( this.video );

        // Load the video
        this.video.load();

        this.video.addEventListener( "loadedmetadata",  (e) => {
            console.log( this.video.duration );
            this.video.play();
            this.runProgressbar( this.video.duration * 1000 );
        } );

        // Progressbar
        this.video.addEventListener( "timeupdate", () => {

            this.progressbar.style.width = String( 
                Math.floor( 
                    ( this.video.currentTime / this.video.duration ) 
                    * parseInt( this.controller.element.style.width ) 
                ) 
            ) + "px";

        } );

        this.video.addEventListener( "ended", () => {

            window.Controller.sendShowEnded();

        } );

    }

    onDeactivate(): void {
        if ( this.source !== undefined ) {
            this.source.remove();
        }
        if ( this.video !== undefined ) {
            this.video.remove();
        }
    }

}

export default VideoScreen;