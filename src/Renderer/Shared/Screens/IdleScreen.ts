import AbstractScreen from "../AbstractScreen";
import Controller from "../Controller";
import { RequestParameters } from "../Requests";
import { ScreenOptions } from "../ScreenOptions";

export class IdleScreen extends AbstractScreen
{

    type: string = "idle";

    params: IdleScreenOptions;

    image?: HTMLImageElement;

    constructor( controller: Controller, params: IdleScreenOptions ) {
        super( controller, params );
        console.log( params );
    }

    onBuild(): void {
        
        if ( this.params.image !== undefined ) {
            this.image = document.createElement( "img" );
            this.image.src = this.params.image;
            this.image.classList.add("idle-screen-image");
            this.wrapper.appendChild( this.image );
        }
        
    }

    onRemove(): void {
        if ( this.params.image !== undefined ) {
            this.image.remove();
        }
    }

    onActivate( requestParameters: RequestParameters ): void {
        console.log( "tak co?" );
    }

    onDeactivate(): void {
        
    }

}

export interface IdleScreenOptions extends ScreenOptions
{
    image?: string
}

export interface IdleScreenRequest extends RequestParameters
{
    image?: string
}