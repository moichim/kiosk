import { ConfigInterface } from "../../Main/Storage";
import AbstractScreen from "./AbstractScreen";
import { RequestParameters } from "./Requests";
import { ScreenOptions } from "./ScreenOptions";
import CompositeScreen from "./Screens/CompositeScreen";
import { IdleScreen, IdleScreenOptions } from "./Screens/IdleScreen";
import ImageScreen from "./Screens/ImageScreen";
import LinearScreen from "./Screens/LinearScreen";
import ScreensaverScreen, { ScreensaverScreenOptions, ScreensaverScreenRequest } from "./Screens/ScreensaverScreen";
import SimultaneousScreen from "./Screens/SimultaneousScreen";
import TouchScreen, { TouchScreenOptions } from "./Screens/TouchScreen";
import TwoImagesScreen from "./Screens/TwoImagesScreen";
import VideoScreen from "./Screens/VideoScreen";

const { ipcRenderer } = window.require( "electron" );

class Controller {

    element: HTMLElement;

    screens: {
        [index: string]: AbstractScreen
    } = {};

    target: "projector"|"tablet";

    config: ConfigInterface;

    timer: NodeJS.Timer;


    /**
     * 
     * @param app_conteiner_ID DOM ID from the template
     * @param target Indicate what should be the target of its request
     */
    constructor(
        app_conteiner_ID: string,
        target: "projector"|"tablet"
    ) {

        this.element = document.getElementById( app_conteiner_ID );

        this.target = target;

    }

    setConfig( config:ConfigInterface ): void
    {
        this.config = config;
    }


    createScreen( config: ScreenOptions ): AbstractScreen
    {

        let screen: AbstractScreen = null;

        if ( config.type === "image" ) {
            screen = new ImageScreen( this, config );
        } else if ( config.type === "video" ) {
            screen = new VideoScreen( this, config );
        } else if ( config.type === "screensaver" ) {
            screen = new ScreensaverScreen( this, config as ScreensaverScreenOptions );
        } else if ( config.type === "composite" ) {
            screen = new CompositeScreen( this, config );
        } else if ( config.type === "linear" ) {
            screen = new LinearScreen( this, config );
        } else if ( config.type === "simultaneous" ) {
            screen = new SimultaneousScreen( this, config );
        } else if ( config.type === "idle" ) {
            screen = new IdleScreen( this, config );
        } else if ( config.type === "twoimages" ) {
            screen = new TwoImagesScreen( this, config );
        }

        // Assign the item to the container
        if ( screen !== null ) {
            this.screens[ screen.id ] = screen;
        }

        return screen;

    }


    activateScreen( id: string, requestParameters: RequestParameters = {} ): void
    {

        for ( let screen in this.screens ) {

            let scr: AbstractScreen = this.screens[ screen ];

            if ( scr.id === id ) {
                scr.activate( requestParameters );
            } else {
                scr.deactivate();
            }

        }

    }


    /**
     * A shorthand for activating screensaver easily.
     * @param heading 
     * @param subheading 
     */
    activateScreensaver(): void
    {
        this.activateScreen( "idle", {} );
    }


    activateTouchControl(): void
    {
        this.activateScreen( "touch", {} );
        this.timerStart();
    }

    timerStart(): this
    {

        if ( this.timer !== undefined ) {
            clearTimeout( this.timer );
        }

        this.timer = setTimeout( () => {
            this.activateScreensaver();
            clearTimeout( this.timer );
            this.timer = undefined;
        }, 0.5 * 60 * 1000 );

        return this;
    }

    timerStop(): this
    {

        if ( this.timer !== undefined ) {
            clearTimeout( this.timer );
            this.timer = undefined;
        }

        return this;
    }




    /**
     * Shorthand of the TouchControl creation
     */
    addTouchScreen(): TouchScreen
    {

        const params: TouchScreenOptions = {
            type: "touch",
            id: "touch",
            background: this.config.tablet.background,
            screens: this.config.screens
        };

        const screen = new TouchScreen( this, params );

        this.screens[ "touch" ] = screen;

        return screen;

    }

    addScreensaverScreen( image: string = undefined ): IdleScreen
    {
        const params = {
            image: image,
            type: "idle",
            id: "idle"
        } as IdleScreenOptions;

        const screen = new IdleScreen( this, params );

        this.screens[ "idle" ] = screen;

        return screen;


    }


    /**
     * Request something on the other projector.
     * @param id 
     * @param requestParameters 
     */
    sendRequest(
        id: string,
        requestParameters: RequestParameters = {}
    ): void 
    {

        let request: RequestParameters = {
            id: id,
            target: this.target,
            ...this.config.data[ id ],
            ...requestParameters
        }

        ipcRenderer.send( "signalise", request );

    }

    sendShowEnded() {

        ipcRenderer.send( "show-ended", {} );

        this.activateScreensaver();

    }


    /**
     * Reset the entire DOM elements & controller classes.
     */
    reset(): void
    {
        for ( let screen in this.screens ) {

            let scr: AbstractScreen = this.screens[ screen ];

            scr.remove();

        }

        this.screens = {};

    }



}

export default Controller;