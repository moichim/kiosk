import AbstractScreen from "../AbstractScreen";
import { RequestParameters } from "../Requests";
import { FileScreenOptions, ScreenOptions, TriggerableScreenOptions } from "../ScreenOptions";

class TwoImagesScreen extends AbstractScreen
{

    type: string = "twoimages";

    defaults: {
        file: "bloud.jpg"
    }

    params: TwoFileScreenOptions;

    one: HTMLImageElement;
    two: HTMLImageElement;

    timer: NodeJS.Timer;
    halftimer: NodeJS.Timer;

    onBuild(): void
    {

        this.one = document.createElement( "img" );
        this.one.classList.add( "two-image" );
        this.one.classList.add( "two-image__first" );
        this.one.setAttribute( "src", this.params.one );
        this.wrapper.appendChild( this.one );

        this.two = document.createElement( "img" );
        this.two.classList.add( "two-image" );
        this.two.classList.add( "two-image__second" );
        this.two.setAttribute( "src", this.params.two );
        this.wrapper.appendChild( this.two );
    }

    onRemove(): void
    {
        this.one.remove();
        this.two.remove();
    }

    onActivate( requestParameters: ImageRequestParameters = { duration: 3000 } ): void {

        this.container.classList.remove( "second" );
        this.container.classList.add( "first" );

        this.runProgressbar( requestParameters.duration );

        this.timer = setTimeout( () => {

            window.Controller.sendShowEnded();

        }, requestParameters.duration );

        this.halftimer = setTimeout( () => {

            this.container.classList.remove( "first" );
            this.container.classList.add( "second" );

        }, requestParameters.duration / 2 );
        
    }

    onDeactivate(): void {
        this.progressbar.style.width = "0px";
        if ( this.timer !== undefined ) {
            clearTimeout( this.timer );
            this.timer = undefined;
        }
        if ( this.halftimer !== undefined ) {
            clearTimeout( this.halftimer );
            this.halftimer = undefined;
        }
        this.container.classList.remove( "second" );
        this.container.classList.remove( "first" );
    }

}

export default TwoImagesScreen;

export interface TwoFileScreenOptions extends ScreenOptions, TriggerableScreenOptions {
    one: string,
    two: string
}

export interface ImageRequestParameters extends RequestParameters
{
    duration: number
}