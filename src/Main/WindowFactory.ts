import { BrowserView, BrowserWindow, screen, BrowserWindowConstructorOptions, Display } from "electron";

const defaults: BrowserWindowConstructorOptions = {
    webPreferences: {
        nodeIntegration: true,
        contextIsolation: false,
        webSecurity: false
    }
};

class WindowFactory {

    static create( params: WindowParams ): BrowserWindow
    {

        let options = Object.assign( defaults, params );

        if ( params.debug === false ) {
            options.width = params.device.workAreaSize.width;
            options.height = params.device.workAreaSize.height;
            options.x = params.device.workArea.x;
            options.y = params.device.workArea.y;
            options.movable = false;
            options.titleBarStyle = "hidden";
            options.autoHideMenuBar = true;
            options.fullscreen = true;
        }

        let window = new BrowserWindow( options );

        window.loadURL( params.webpack_entry );

        if ( params.debug !== undefined && params.debug === true ) {
            // window.webContents.openDevTools();
        }

        return window;
    }

    static getDisplays(): DisplaysConfig
    {

        let displays = screen.getAllDisplays().filter( scr => { return scr.id !== screen.getPrimaryDisplay().id; } );

        const config: DisplaysConfig = {
            primary: screen.getPrimaryDisplay(),
            secondary: displays.length > 0
                ? displays[0]
                : screen.getPrimaryDisplay()
        }

        return config;
    }

}

export interface WindowParams extends BrowserWindowConstructorOptions{
    
    /** Webpack entry file constant. */
    webpack_entry: string,
    
    /** Should we open the window in debug mode? */
    debug?: boolean,

    /** On which device shoudl I display this window? */
    device: Display

};

interface DisplaysConfig {
    /** The primary display. */
    primary: Display,
    /** If present, this is the secondary display. */
    secondary: Display
}

export default WindowFactory;