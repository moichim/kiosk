#!/bin/sh

(
while true; do
	res=$(xrandr | grep "HDMI-2 disconnected")
	if [ -z "$res" ] ; then
		break
	fi
	sleep 1
done
) |
zenity --progress --auto-close --no-cancel --pulsate --text="Waiting for HDMI-2 ..."

xrandr --output HDMI-2 --auto
xrandr --output HDMI-2 --right-of HDMI-1

#ID=$(xinput --list --id-only "eGalax Inc. eGalaxTouch P80H84 2583 v02 k4.10.143")
ID=$(DISPLAY=:0 xinput --list | grep -m 1 "eGalax" | sed -n 's/.*id=\([0-9]\+\).*/\1/p')
xinput map-to-output $ID HDMI-1

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" # This loads nvm bash_completion

# Load the Nodejs version
nvm use 16.13.2

# Run the electron app
npm run start

