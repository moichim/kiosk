const randomStringBase = [..."abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ];


export const getRandomString = ( num: number = 5 ): string => {
    return [ ...Array( num ) ].map( i => {
        return [...randomStringBase][ Math.random()*randomStringBase.length|0 ];
    } ).join( "" );
}