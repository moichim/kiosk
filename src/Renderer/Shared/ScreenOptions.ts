/**
 * 
 * All screens are instantiated through ScreenOptions objects or their derivates.
 * 
 * @author Jan Jáchim
 */


/**
 * Base request for presentation must contain the ID.
 */
 export interface ScreenOptions {
    type: "image"|"video"|"screen"|"screensaver"|"touch"|"composite"|"linear"|"simultaneous"|"idle"|"twoimages",
    id?: string,
    [index: string] : any
}

/**
 * Every option that is triggerable must have title and dimension of the trigger
 */
export interface TriggerableScreenOptions extends ScreenOptions {
    
    /** The title is obligatory */
    title: {
        cz: string,
        en: string
    }

    icon_trigger: string,
    icon_screen: string,

    /** Parameters of the trigger */
    trigger: {
        x: number,
        y: number,
        width: number,
    }

}

/**
 * Base for all file based requests
 */
export interface FileScreenOptions extends ScreenOptions, TriggerableScreenOptions {
    file: string
}