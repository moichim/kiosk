

/**
 * Each request between tablet and projector must be an instance of signal
 */
export interface RequestParameters extends Object {
    /** In most cases, this is obligatory */
    id?: string,
    /** Target scope shall be used in the main process to route responses into particular windows */
    target?: "projector"|"tablet"
}