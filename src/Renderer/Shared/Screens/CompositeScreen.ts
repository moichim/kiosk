import AbstractScreen from "../AbstractScreen";
import { RequestParameters } from "../Requests";
import { TriggerableScreenOptions } from "../ScreenOptions";

/**
 * @deprecated
 */
class CompositeScreen extends AbstractScreen
{

    params: CompositeScreenOptions;

    defaults: Object = {
        overlap: 3000,
        position: {
            x: 100,
            y: 100
        },
        width: 100
    }

    image: HTMLDivElement;
    video: HTMLVideoElement;
    source: HTMLSourceElement;
    videoContainer: HTMLDivElement;

    icon: HTMLImageElement;

    timer_total: NodeJS.Timer;
    timer_video: NodeJS.Timer

    onBuild(): void {

    }

    onRemove(): void {
        
    }

    onActivate(requestParameters: CompositeRequestParameters): void {
        
        // Build the image
        this.image = document.createElement( "div" );
        this.image.classList.add( "composite-image" );

        let image = document.createElement( "img" );
        image.setAttribute( "src", this.params.image );

        this.image.appendChild( image );

        // Build the video

        this.videoContainer = document.createElement( "div" );
        this.videoContainer.classList.add( "composite-video-container" );

        this.videoContainer.style.top = this.params.position.x + "px";
        this.videoContainer.style.left = this.params.position.y + "px";
        this.videoContainer.style.width = this.params.width + "px";

        this.video = document.createElement( "video" );
        this.video.classList.add( "composite-video" );
        this.video.muted = true;

        // Build the source
        this.source = document.createElement( "source" );
        this.source.classList.add( "composite-source" );
        this.source.setAttribute( "type", "video/mp4" );
        this.source.setAttribute( "src", this.params.video );

        // Compose the real DOM
        this.video.appendChild( this.source );
        this.videoContainer.appendChild( this.video );
        this.wrapper.appendChild( this.image );
        this.wrapper.appendChild( this.videoContainer );

        // Load the video
        this.video.load();

        this.video.addEventListener( "loadedmetadata",  (e) => {

            this.video.play();

            const dur = ( this.video.duration * 1000 ) + this.params.overlap;

            this.runProgressbar( dur );

            this.timer_total = setTimeout( () => {
                window.Controller.sendShowEnded();
            }, dur );

            this.timer_video = setTimeout( () => {
                this.videoContainer.classList.add( "composite-video-container__ended" );
                this.progressbar.classList.add( "screen-progressbar__overlap" );
            }, ( this.video.duration * 1000 ) - 300 );

        } );

    }

    onDeactivate(): void {
        if ( this.source !== undefined ) {
            this.source.remove();
        }
        if ( this.video !== undefined ) {
            this.video.remove();
        }
        if ( this.videoContainer !== undefined ) {
            this.videoContainer.remove();
        }
        if ( this.image !== undefined ) {
            this.image.remove();
        }

        this.progressbar.classList.remove( "screen-progressbar__overlap" );

        if ( this.timer_total !== undefined ) {
            clearTimeout( this.timer_total );
            this.timer_total = undefined;
        }

        if ( this.timer_video !== undefined ) {
            clearTimeout( this.timer_video );
            this.timer_video = undefined;
        }

    }
}

export default CompositeScreen;

export interface CompositeScreenOptions extends TriggerableScreenOptions {
    video: string,
    image: string,
    position?: {
        x: number,
        y: number
    },
    width?: string,
    overlap?: number,
}

export interface CompositeRequestParameters extends RequestParameters {
    overlap: number,
    video: string,
    image: string,
    position: {
        x: number,
        y: number
    },
    width: number
}